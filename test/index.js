'use strict';

const assert = require('node:assert');
const { setImmediate } = require('node:timers/promises');
const { create:service } = require('..');
const EventEmitter = require('node:events');

describe('Rinse And Repeat Service', function (){
	before(function arrangeStartService (done){
		const self = this;
		self.logs = [];
		let start = Date.now();
		async function work (params){

			self.params = params;

			self.params.eventEmitter.emit('work', true);
			self.logs.push(Date.now() - start);
			await setImmediate();
		}
		self.service = service({ work, repeatEvery:100 });
		const otherServices = { a:1 };

		self.service.start(otherServices, done);
	});
	before(function failOnDoubleStart (done){
		const self = this;
		const { service } = self;
		service.start(null, function (err){
			self.alreadyStarted = err;
			return done();
		});
	});
	before(function listenToClientEvents (){
		const self = this;
		const { service } = self;
		service.getClient().on('work', () => self.events = true);
	});
	before(async function arrangeWaitTillSomeWorkDone (){
		const { logs, service } = this;
		while(logs.length<3){
			await setImmediate();
		}
		service.stop();
	});

	it('should do some work then wait N ms and repeat', function (){
		const { logs } = this;
		let prev = logs[0];
		let i;
		for(i=1; i < logs.length; i++){
			let current = logs[i];
			assert(prev + 100 <= current);
			prev = current;
		}
		assert(i>2, 'tests skipped');
	});
	describe('Arguments passed to the function',function (){
		it('should pass the required services', function (){
			const { params: { dependencies } } = this;
			assert.deepStrictEqual(dependencies, { a:1 });
		});
		it('should pass the event emitter', function (){
			const { events, params:{ eventEmitter } } = this;
			assert(eventEmitter instanceof EventEmitter);
			assert(events);
		});
		it('should pass the abort signal', function (){
			const { params: { signal } } = this;
			assert(signal instanceof AbortSignal);
		});
	});
	describe('Trying to start (when already running)', function (){
		it('should callback an error', function (){
			const { alreadyStarted:err } = this;
			assert.deepStrictEqual(err.message, 'Already started');
		});
	});
	it('Calling #stop before start should not fail', function (done){
		let work = () => {};
		service({ work }).stop(done);
	});
});
