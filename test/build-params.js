'use strict';

const assert = require('node:assert');
const buildParams = require('../lib/build-params');

describe('#buildParams(defaults, options)', function (){
	it('should override #defaults with #options', function (){
		let fun = async () => {};
		let { name, repeatEvery, work } = buildParams({ name: 'a', repeatEvery: 100 }, { name: 'ok', work:fun });
		assert.deepStrictEqual(name, 'ok');
		assert.deepStrictEqual(repeatEvery, 100);
		assert.deepStrictEqual(work, fun);
	});
	it('should fail if #work is not a function', function (){
		try{
			buildParams({ name: 'a', repeatEvery: 100 }, { name: 'ok', work:1 });
		}catch(e){
			assert.deepStrictEqual(e.message, '"work" is required and must be a function');
			return;
		}
		throw new Error('unexpected code branch');
	});
	it('should fail if #name is not provided', function (){
		let work = async () => {};
		try{
			buildParams({ repeatEvery: 100 }, { work });
		}catch(e){
			assert.deepStrictEqual(e.message, '"name" is required');
			return;
		}
		throw new Error('unexpected code branch');
	});
	it('should fail if "repeatEvery" is not an integer', function (){
		let work = async () => {};
		try{
			buildParams({ repeatEvery: '100' }, { name:'ok', work });
		}catch(e){
			assert.deepStrictEqual(e.message, '"repeatEvery" must be an integer');
			return;
		}
		throw new Error('unexpected code branch');
	});
	it('should fail when "repeatEvery" is lower than 100', function (){
		let work = async () => {};
		try{
			buildParams({ repeatEvery: 99 }, { name:'ok', work });
		}catch(e){
			assert.deepStrictEqual(e.message, '"repeatEvery" must be greater or equal than 100 miliseconds');
			return;
		}
		throw new Error('unexpected code branch');
	});
});
