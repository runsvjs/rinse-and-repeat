'use strict';

const assert = require('node:assert');
const ensureAsync = require('../lib/ensure-async');

describe('#ensureAsync(fn)', function (){
	describe('Given an async (callback) function is provided a promise is returned', function (){
		it('should pass signal, eventEmitter and other services', async function (){
			const otherServices = {};
			const ev = {};
			const signal = {};
			function fn (_otherServices, _ev, _signal, callback){
				// double check params are passed
				assert(_otherServices === otherServices);
				assert(_ev === ev);
				assert(_signal === signal);

				return callback();
			}

			await ensureAsync(fn)(otherServices, ev, signal);
		});
		it('should handle errors', async function (){
			let expected = new Error('expected');
			function fn (callback){
				return callback(expected);
			}

			try{
				await ensureAsync(fn)();
			}catch(e){
				assert(e === expected);
				return;
			}
			throw new Error('Unexpected code branch');
		});
	});
});
