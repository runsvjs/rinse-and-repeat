'use strict';

const assert = require('assert');
const sleep = require('../lib/sleep');

describe('#sleep(signal, ms)', function (){
	it('should sleep N milliseconds', async function (){
		let start = Date.now();
		await sleep(null, 300);
		const end = Date.now();
		assert(end - start >= 300);
		assert(end - start < 310);
	});
	it('should stop when abort signal is triggered', function (done){
		let start = Date.now();
		const controller = new AbortController();
		sleep(controller.signal, 300).then(() => {
			const end = Date.now();
			assert(end - start >= 200);
			assert(end - start < 210);
			done();
		}).catch(done);
		setTimeout(() => controller.abort(), 100);
	});
});
