'use strict';
const assert = require('assert');
const ensureAsync = require('./ensure-async');

/**
 * Params definition
 * @property {String} name - Service name
 * @property {Number} repeatEvery - Time to repeatEvery until next run. In milliseconds.
 * @property {Function} work - Async function to run. Async or Promise or Callback interface.
 */
class Params {
	name;
	repeatEvery;
	work;
	constructor (name, repeatEvery, work){
		this.name = name;
		this.repeatEvery = repeatEvery;
		this.work = work;
	}
}

/**
 * buildParams.
 *
 * @param {object} defaults
 * @param {object} options
 * @returns {Params}
 */
function buildParams (defaults, options){
	const { name, repeatEvery, work } = { ...defaults, ...options };

	assert(work instanceof Function, '"work" is required and must be a function');
	assert(name, '"name" is required');
	assert(Number.isInteger(repeatEvery), '"repeatEvery" must be an integer');
	assert(repeatEvery>= 100, '"repeatEvery" must be greater or equal than 100 miliseconds');

	return new Params(name, repeatEvery, ensureAsync(work));
}

module.exports = buildParams;
