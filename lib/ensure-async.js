'use strict';

function ensureAsync (fn) {
	if (fn.constructor.name === 'AsyncFunction') {
		return fn;
	}
	return function providedFnAsPromise (...args) {
		return new Promise(function (resolve, reject) {
			fn(...args, function (err) {
				if (err) {
					return reject(err);
				}
				return resolve();
			});
		});
	};
}

module.exports = ensureAsync;
