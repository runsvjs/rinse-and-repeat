'use strict';

const { setTimeout } = require('node:timers/promises');
const assert = require('node:assert');

async function _sleep (signal, ms) {
	let till = Date.now() + ms;
	while (Date.now() < till && signal?.aborted !== true) {
		await setTimeout(100);
	}
}

/**
 * Sleep for N milliseconds or until aborted signal.
 * Min
 *
 * @param {AbortSignal} signal 
 * @param {number} ms - Number of milliseconds to wait
 */
async function sleep (signal, ms) {

	assert(Number.isInteger(ms), 'ms must be a number');
	assert(ms>= 100, 'ms must be a higher than 100');
	
	ms = Math.max(100, ms);

	await _sleep(signal, ms);
}

module.exports = sleep;

