# Rinse And Repeat

Do some work. Sleep. Repeat.

![npm](https://img.shields.io/npm/v/runsv?style=for-the-badge)
[![pipeline status](https://gitlab.com/runsvjs/rinse-and-repeat/badges/master/pipeline.svg)](https://gitlab.com/runsvjs/rinse-and-repeat/commits/master)
[![coverage report](https://gitlab.com/runsvjs/rinse-and-repeat/badges/master/coverage.svg)](https://gitlab.com/runsvjs/rinse-and-repeat/commits/master)

## Install

`$ npm i runsv-rinse-and-repeat`

## Usage


```javascript
const MINUTE = 60 * 1000;
const runsv = require('runsv');
const createService = require('runsv-rinse-and-repeat');

async function work(params){
	// Do your work
}

const myService = createService({ 
	name: 'myService', 
	repeatEvery: 5 * MINUTE, 
	work 
});

const sv = runsv.create();
sv.addService(myService);
// ...
```
### Params

The params object passed to the worker function has the following properties:

* `name` The name of the service.
* `repeatEvery` Milliseconds to wait between runs. Min. value is 100 milliseconds.
* `eventEmitter` An event emitter you can use.
* `signal` Abort signal you can use to stop your worker. 
* `dependencies` Services 
