'use strict';

const { EventEmitter } = require('node:events');
const sleep = require('./lib/sleep');
const buildParams = require('./lib/build-params');

const REPEAT_EVERY = 60 * 1000;

const defaults = {
	name: 'rinseAndRepeat',
	repeatEvery: REPEAT_EVERY
};

class WorkerParams {
	eventEmitter;
	dependencies;
	signal;
	name;
	repeatEvery;
	constructor ({ name, dependencies, eventEmitter, signal, repeatEvery }){
		this.name = name;
		this.dependencies = dependencies;
		this.eventEmitter = eventEmitter;
		this.signal = signal;
		this.repeatEvery = repeatEvery;
	}
}

function create (options) {
	let controller, ev, done;
	
	const { name, repeatEvery, work } = buildParams(defaults, options);

	const ret = {
		name,
		getClient: () => ev,
		start (otherServices, callback) {

			if (controller) {
				return callback(new Error('Already started'));
			}

			controller = new AbortController();
			ev = new EventEmitter();
			// placeholder. The real callback will be set up by #stop
			done = () => {};

			async function rinseAndRepeat () {
				while (controller.signal?.aborted !== true) {
					const params = new WorkerParams({
						name,
						repeatEvery,
						eventEmitter: ev,
						dependencies: otherServices,
						signal: controller.signal
					});
					await work(params);
					await sleep(controller.signal, repeatEvery);
				}
			}

			rinseAndRepeat()
				.then(() => done())
				.catch(e => ev.emit('error', e));

			setImmediate(callback);
		},
		stop (callback) {
			if(!controller){
				setImmediate(callback);
				return;
			}

			done = callback;
			controller.abort();
		}
	};
	return ret;
}
module.exports = { create, WorkerParams };
